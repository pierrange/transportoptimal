/**
 * \file main.c
 * \brief Programme d'Optimisation du Transport.
 * \author LEUNDEU.H
 * \version 0.2
 * \date 22 decembre 2019
 * Transfert optimal de matiere des sites de production vers les sites de demande:On recherche le transport le moins couteux.
 */

#include "procedure_et_type.h"

int main()
{
    int choix;
    int N_p,N_d;///variables qui contiennent respectivement le nombre de sites de production et le nombre de sites de demande
    printf("voulez vous entrer vos valeurs par vous meme ? :\n\t 0) Non je laisse l'ordinateur s'en occuper \n\t 1) OUI je veux entrer mes valeurs\n");
    scanf("%d",&choix);

    long int u;
    time(&u);
    srand(u);///definition de l'element permettant de generer les valeurs aleatoires entre 0 et 1


    printf("entrer le nombre de sites de production\n");
    scanf("%d",&N_p);
    printf("entrer le nombre de sites de demande\n");
    scanf("%d",&N_d);
    site *site_production;///definie un tableau contenant les sites de production
    site *site_demande;///definie un tableau contenant les sites de demande
    ///allocation des pointeurs site_demande et site_production
    site_demande=(site*)malloc(N_d*sizeof(site));
    site_production=(site*)malloc(N_p*sizeof(site));

    ///declaration et allocation de la matrice transport
    float **transport;
    transport=(float**)malloc(N_p*sizeof(float*));
    for(int j=0; j<N_p; j++)
    {
        transport[j]=(float*)malloc(N_d*sizeof(float));
    }
    ///constitution de la matrice cout_transport
    cout_min cout_transport[N_p][N_d];
    for(int i=0; i<N_p; i++)
    {
        site_production[i].X=1*((float)rand())/RAND_MAX;///l'abscisse du site de production numero i
        site_production[i].Y=1*((float)rand())/RAND_MAX;///l'ordonn�e du site de production numero i
    }
    for(int i=0; i<N_d; i++)
    {
        site_demande[i].X=1*((float)rand())/RAND_MAX;///l'abscisse du site de demande numero i
        site_demande[i].Y=1*((float)rand())/RAND_MAX;///l'ordonn�e du site de demande numero i
    }
    ///definition et initialisation des variables utilise au remplissage des tableaux de demande et de production
    int indice=0,position_min;
      float prod_limite;
    prod_limite = N_p*1.0;
    printf("\n la production limite est : %f",prod_limite);
    float valeur;
    if(choix==0)
    {
            float volume = 0;
            for(int i=0 ;i<N_d;i++){
                valeur= (rand()/(float)RAND_MAX);
                while((valeur >= prod_limite && i != N_d-1) || (valeur> prod_limite && i == N_d-1)){
                    valeur = (rand()/(float)RAND_MAX);

                }
                site_demande[i].quantite = valeur;
                prod_limite -= valeur;
                volume += site_demande[i].quantite;
            }
            // initialisons les volumes de production
            float reste = volume;
            for(int i=0;i<N_p-1;i++){
                valeur = (rand()/(float)RAND_MAX);
                while(valeur > reste || (reste-valeur)> N_p-i-1){
                    valeur = (rand()/(float)RAND_MAX);
                }
                site_production[i].quantite = valeur;
                reste-= valeur;
            }
            site_production[N_p-1].quantite = reste;
    }
    else
    {
            float volume = 0;
            prod_limite = N_p*1.0;
            for(int i = 0;i<N_d;i++){
                printf("\n rentrer le volume de demande pour le site %d", i+1);
                scanf("%f",&valeur);
                while(valeur>1 || (valeur >= prod_limite && i != N_d-1) || (valeur> prod_limite && i == N_d-1)){
                    printf(" \n rentrer un volume entre 0 et 1");
                    scanf("%f",&valeur);
                }
                site_demande[i].quantite = valeur;
                volume += valeur;
                prod_limite -= valeur;
            }
            // Les volumes de production
            float reste2 = volume;
            for (int i=0;i<N_p-1;i++){
                printf(" \n rentrer le volume de prodution pour le site %d", i+1);
                scanf("%f",&valeur);
                while(valeur>1 || valeur> reste2 || (reste2-valeur)> N_p-i-1){
                    printf(" \n rentrer le volume de production entre o et 1 et inf�rieur au reste de la demande(%f): ",reste2);
                    scanf("%f",&valeur);
                }
                site_production[i].quantite = valeur;
                reste2 -= valeur;
            }
            printf(" \n Le volume de production du dernier site sera %f pour �galiser avec la demande", reste2);
            site_production[N_p-1].quantite = reste2;
    }

    printf("\n\nles quantites des sites de demande sont respectivement : \n");
    for(int i=0; i<N_d; i++)
    {
        printf("\t %.3f",site_demande[i].quantite);
    }
    printf("\n\nles quantites des sites de production sont respectivement : \n");
    for(int i=0; i<N_p; i++)
    {
        printf("\t %.3f",site_production[i].quantite);
    }
    printf("\n\n");




    for(int i=0; i<N_p; i++)
    {
        ///calcul des couts de transport et enregistrement dans la matrice

        for(int j=0; j<N_d; j++)
        {
            cout_transport[i][j].valeur=pow(site_production[i].X-site_demande[j].X,2);
            cout_transport[i][j].valeur+=pow(site_production[i].Y-site_demande[j].Y,2);
            cout_transport[i][j].ligne=i+1;
            cout_transport[i][j].colonne=j+1;
        }
    }
    cout_min minim;
    cout_min matrice_reduit[N_p][N_d];
    printf("La matrice du cout de transport entre les sites de production (ligne) et les sites de demande (colonne): \n ");
    afficher_matrice(N_p,N_d,cout_transport);
    copier(N_p,N_d,cout_transport,matrice_reduit);
    printf("\n\n");

    for(int i=0; i<N_p; i++)
    {
        for(int j=0; j<N_d; j++)
        {
            transport[i][j]=0;
        }
    }

    int i=0,j=0;
    float valeur_recuperer;
    int vrai,sortir_boucle;
    for(int col=0; col<N_d; col++)
    {
        i=0,j=0;
        sortir_boucle=0;
        copier(N_p,N_d,cout_transport,matrice_reduit);
        while(site_demande[col].quantite!=0 && sortir_boucle==0)
        {
            minim=minimum(N_p-i,N_d-j,matrice_reduit);
            if(site_demande[minim.demande-1].quantite==site_production[minim.prod-1].quantite)
            {
                if((minim.ligne==0 && N_p-i==1) || (minim.colonne==0 && N_p-j==1))
                {
                    sortir_boucle=1;
                    goto suivant_ij;
                }
                valeur_recuperer=site_demande[minim.demande-1].quantite;
                vrai=0;
                printf("\n Nous allons reduire la matrice en eliminant la ligne %d et la colonne\n",minim.ligne+1,minim.colonne+1);
                afficher_matrice(N_p-i,N_d-j,matrice_reduit);
                printf("\n");
                reduire_matrice(N_p-i,N_d-j,minim.ligne,minim.colonne,matrice_reduit,matrice_reduit);
                i++;
                j++;
                suivant_ij :
                    printf("");
            }
            else if(site_demande[minim.demande-1].quantite>site_production[minim.prod-1].quantite)
            {
                if(minim.ligne==0 && N_p-i==1)
                {
                    sortir_boucle=1;
                    goto suivant;
                }
                valeur_recuperer=site_production[minim.prod-1].quantite;
                vrai=1;
                printf("\nNous allons reduire la matrice en eliminant la ligne %d\n",minim.ligne+1);
                afficher_matrice(N_p-i,N_d-j,matrice_reduit);
                printf("\n");
                reduire_ligne_matrice(N_p-i,N_d-j,minim.ligne,matrice_reduit,matrice_reduit);
                i++;
                suivant :
                    printf("");
            }
            else
            {
                if(minim.colonne==0 && N_d-j==1)
                {
                    sortir_boucle=1;
                    goto suivant_j;
                }
                valeur_recuperer=site_demande[minim.demande-1].quantite;
                vrai=2;
                printf("\nNous allons reduire la matrice en eliminant la colonne %d\n",minim.colonne+1);
                afficher_matrice(N_p-i,N_d-j,matrice_reduit);
                printf("\n");
                reduire_colonne_matrice(N_p-i,N_d-j,minim.colonne,matrice_reduit,matrice_reduit);
                j++;
                suivant_j :
                    printf("");
            }
            if(minim.demande-1==col)
            {
                if(vrai==0)
                {
                    site_demande[col].quantite=0;
                    site_production[minim.prod-1].quantite=0;
                }
                else if(vrai==1)
                {
                    site_demande[col].quantite-=valeur_recuperer;
                    site_production[minim.prod-1].quantite=0;
                }
                else
                {
                    site_demande[col].quantite=0;
                    site_production[minim.prod-1].quantite-=valeur_recuperer;
                }
                transport[minim.prod-1][col]=valeur_recuperer;
            }
        }
    }

    float distance_max;
    int position_distance_min;
    for(int i=0; i<N_d; i++)
    {
        valeur=0;
        for(int j=0; j<N_p; j++)
        {
            valeur+=transport[j][i];
        }
        if(valeur==0)
        {
            do
            {
                distance_max=2;
                position_distance_min=0;
                for(int j=0; j<N_p; j++)
                {
                    if(site_production[j].quantite>0 && cout_transport[j][i].valeur<distance_max)
                    {
                        position_distance_min=j;
                    }
                }
                if(site_production[position_distance_min].quantite<site_demande[i].quantite)
                {
                    transport[position_distance_min][i]+=site_production[position_distance_min].quantite;
                    site_production[position_distance_min].quantite==0;
                    site_demande[i].quantite-=site_production[position_distance_min].quantite;
                }
                else if(site_production[position_distance_min].quantite>site_demande[i].quantite)
                {
                    transport[position_distance_min][i]+=site_demande[i].quantite;
                    site_production[position_distance_min].quantite-=site_demande[i].quantite;
                    site_demande[i].quantite=0;
                }
                else
                {
                    transport[position_distance_min][i]+=site_demande[i].quantite;
                    site_production[position_distance_min].quantite=0;
                    site_demande[i].quantite=0;
                }
                valeur=0;
                for(int p=0;p<N_p;p++)
                {
                    valeur+=site_production[p].quantite;
                }

            }
            while(site_demande[i].quantite!=0 && valeur>0.1);
        }

    }

    printf("\n\n");
    float cout_total=0;
    printf("la matrice finale du transport (la ligne i approvisionne la ligne j de la quantite en (i,j))est:");
    for(int i=0; i<N_p; i++)
    {
        for(int j=0; j<N_d; j++)
        {
            if(j%N_d==0)
            {
                printf("\n\t%.3f",transport[i][j]);
            }
            else
            {
                printf("  %.3f",transport[i][j]);
            }

            if(transport[i][j]!=0)
            {
                cout_total+=(transport[i][j]*cout_transport[i][j].valeur);
            }
        }
    }

    printf("\n\n le cout total de transport de cette transaction est %f\n",cout_total);

    printf("\n\n");
    visualiser_resultat(site_production,site_demande,transport,N_p,N_d,"visualiser.jpg");
    for(int j=0; j<N_p; j++)
    {
        free(transport[j]);
    }
    free(transport);
    free(site_demande);///on libere la memoire
    free(site_production);///on libere la memoire
    return 0;
}
