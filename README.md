# Transport Optimal

Le code présente une solution d'un problème de logique qu'est le Transport Optimal.
## Accès

Pour accéder au code il faut entrer dans le fichier "main.c"
Pour l'exécuter il faut télécharger le fichier "main.exe".
```bash
main.c // main.exe
```

## Utilisation

```
Plusieurs commentaires ont été mis dans le code pour vous permettre de comprendre les différentes lignes de codes.
 ```

## Contribution
Le projet a été fait dans un cadre académique à l'Université de Technologie de Troyes.
## License
[UTT](https://www.utt.fr/)