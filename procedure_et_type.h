#ifndef SIMPLEX_H_INCLUDED
#define SIMPLEX_H_INCLUDED


/**
 * \file procedure_et_type.h
 * \brief Bibliotheque de fonctions, procedures et types.
 * \author LEUNDEU.H TAYO.H
 * \version 0.2
 * \date 22 decembre 2019
 *
 * Programme qui contient les sous_algorithmes et les types utilises.
 *
 */
#endif // SIMPLEX_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <mgl2/mgl.h>
/**
 * \struct cout_min procedure_et_type.h
 * \brief Objet qui permet de definir le minimum d'une matrice et les elements des matrices successives .
 *
 *cout_min pour conserver la ligne, la colonne, le site de production et le site de demande correspondant � une valeur dans une matrice.
 */

typedef struct /*definition d'une structure pour prendre la valeur minimale de cout de transport dans une matrice d'element ainsi que les indices*/
{
    float valeur;//la valeur
    int ligne;//ligne dans la matrice iniiale par rapport � la matrice reduit
    int colonne;//colonne dans la matrice iniiale par rapport � la matrice reduit
    int prod;//colonne initiale dans la matrice iniiale
    int demande;//colonne initiale dans la matrice iniiale
} cout_min;

/**
 * \struct site procedure_et_type.h
 * \brief Objet qui permet de definir un site.
 *
 *site pour conserver l'abcisse , l'ordonnee, et la quantite(produite ou demandee) correspondant a un site.
 */
typedef struct
{
    float X;//abscisses
    float Y;//ordonn�es
    float quantite;//la quantit� que veux le site ou que le site peut disposer
} site;
/**
 * \fn afficher_matrice(int ligne,int colonne,cout_min matrice[][colonne])
 * \brief Procedure qui affiche une matrice
 * \param ligne Nombre de lignes de la matrice a afficher qui est une valeur entiere
 * \param colonne Nombre de colonnes de la matrice � afficher qui est une valeur entiere
 * \param matrice Matrice � afficher, tableau bidemensionnel de type cout_min
 *
 */

void afficher_matrice(int ligne,int colonne,cout_min matrice[][colonne])
{
    for(int i=0; i<ligne; i++)
    {
        for(int j=0; j<colonne; j++)
        {
            if(j%colonne==0)
                printf("\n\t%.2f",matrice[i][j].valeur);
            else
                printf("  %.2f",matrice[i][j].valeur);
        }
    }
}
/**
 * \fn cout_min minimum(int ligne,int colonne,cout_min C[][colonne])
 * \brief Fonction qui calcule et renvoie le minimum d'une matrice
 *
 * \param ligne Nombre de lignes de la matrice a traiter qui est une valeur entiere
 * \param colonne Nombre de colonnes de la matrice a traiter qui est une valeur entiere
 * \param C Matrice � reduire, tableau bidemensionnel de type cout_min
 *
 * \return La valeur minimale trouvee de type cout_min
 */
cout_min minimum(int ligne,int colonne,cout_min C[][colonne])
{
    cout_min minimum;
    minimum.valeur=1;
    for(int i=0; i<ligne; i++)
    {
        for(int j=0; j<colonne; j++)
        {
            if(minimum.valeur>=C[i][j].valeur)
            {
                minimum.valeur=C[i][j].valeur;
                minimum.ligne=i;
                minimum.colonne=j;
                minimum.demande=C[i][j].colonne;
                minimum.prod=C[i][j].ligne;
            }
        }
    }
    return minimum;
}
/**
 * \fn reduire_matrice(int ligne,int colonne,int ligne_ini,int colonne_ini,cout_min C[][colonne],cout_min C_reduit[][colonne-1])
 * \brief Procedure qui reduit une matrice d'une ligne et d'une colonne
 *
 * \param ligne Nombre de lignes de la matrice a reduire qui une valeur entiere
 * \param colonne Nombre de colonnes de la matrice a reduire qui est une valeur entiere
 * \param ligne_ini Ligne � supprimer dans la matrice qui une valeur entiere
 * \param colonne_ini  Colonne � supprimer dans la matrice qui est une valeur entiere
 * \param C Matrice � reduire, tableau bidemensionnel de type cout_min
 * \param C_reduit Matrice reduite tableau bidemensionnel de type cout_min
 *
 *
 */
void reduire_matrice(int ligne,int colonne,int ligne_ini,int colonne_ini,cout_min C[][colonne],cout_min C_reduit[][colonne-1])
{
        int i=0,indice_ligne=0,indice_colonne,j;
        while(i<ligne)
        {
            if(i==ligne_ini)
                i++;//ne pas copier cette ligne
            j=0;
            indice_colonne=0;
            while(j<colonne)
            {
                if(j==colonne_ini)
                    j++;//ne pas copier cette colonne
                if(i!=ligne && j!=colonne)
                {
                C_reduit[indice_ligne][indice_colonne].valeur=C[i][j].valeur;
                C_reduit[indice_ligne][indice_colonne].ligne=C[i][j].ligne;
                C_reduit[indice_ligne][indice_colonne].colonne=C[i][j].colonne;
                }
                j++;
                indice_colonne++;
            }
            i++;
            indice_ligne++;
        }

}

/**
 * \fn reduire_ligne_matrice(int ligne,int colonne,int ligne_ini,cout_min C[][colonne],cout_min C_reduit[][colonne])
 * \brief Procedure qui reduit une matrice d'une ligne
 *
 * \param ligne Nombre de lignes de la matrice a reduire qui une valeur entiere
 * \param colonne Nombre de colonnes de la matrice a reduire qui est une valeur entiere
 * \param ligne_ini  Ligne � supprimer dans la matrice qui est une valeur entiere
 * \param C Matrice � reduire, tableau bidemensionnel de type cout_min
 * \param C_reduit Matrice reduite d'une ligne tableau bidemensionnel de type cout_min
 *
 *
 */
void reduire_ligne_matrice(int ligne,int colonne,int ligne_ini,cout_min C[][colonne],cout_min C_reduit[][colonne])
{
        int i=0,indice_ligne=0,indice_colonne,j;
        while(i<ligne)
        {
            if(i==ligne_ini)
                i++;   //ne pas copier cette ligne
            j=0;
            indice_colonne=0;
            while(j<colonne)
            {
                if(i!=ligne)
                {
                C_reduit[indice_ligne][indice_colonne].valeur=C[i][j].valeur;
                C_reduit[indice_ligne][indice_colonne].ligne=C[i][j].ligne;
                C_reduit[indice_ligne][indice_colonne].colonne=C[i][j].colonne;
                }
                j++;
                indice_colonne++;
            }
            i++;
            indice_ligne++;
        }
}

/**
 * \fn reduire_colonne_matrice(int ligne,int colonne,int colonne_ini,cout_min C[][colonne],cout_min C_reduit[][colonne-1])
 * \brief Procedure qui reduit une matrice d'une colonne
 *
 * \param ligne Nombre de lignes de la matrice a reduire qui une valeur entiere
 * \param colonne Nombre de colonnes de la matrice a reduire qui est une valeur entiere
 * \param colonne_ini  Colonne � supprimer dans la matrice qui est une valeur entiere
 * \param C Matrice � reduire, tableau bidemensionnel de type cout_min
 * \param C_reduit Matrice reduite d'une colonne tableau bidemensionnel de type cout_min
 *
 */
void reduire_colonne_matrice(int ligne,int colonne,int colonne_ini,cout_min C[][colonne],cout_min C_reduit[][colonne-1])
{
        int i=0,indice_ligne=0,indice_colonne,j;
        while(i<ligne)
        {
            j=0;
            indice_colonne=0;
            while(j<colonne)
            {
                if(j==colonne_ini)
                    j++;//ne pas copier cette colonne
                if(j!=colonne)
                {
                C_reduit[indice_ligne][indice_colonne].valeur=C[i][j].valeur;
                C_reduit[indice_ligne][indice_colonne].ligne=C[i][j].ligne;
                C_reduit[indice_ligne][indice_colonne].colonne=C[i][j].colonne;
                }
                j++;
                indice_colonne++;
            }
            i++;
            indice_ligne++;
        }
}
/**
 * \fn copier(int ligne,int colonne,cout_min C[][colonne],cout_min C_copie[][colonne])
 * \brief Procedure qui cree une copie d'une matrice
 *
 * \param ligne Nombre de lignes de la matrice a copier qui est une valeur entiere
 * \param colonne Nombre de colonnes de la matrice a copier qui est une valeur entiere
 * \param C Matrice a copier, tableau bidemensionnel de type cout_min
 * \param C_copie Copie de la matrice C, tableau bidemensionnel de type cout_min
 *
 */

void copier(int ligne,int colonne,cout_min C[][colonne],cout_min C_copie[][colonne])
{
    for (int i=0; i<ligne; i++)
    {
        for (int j=0; j<colonne; j++)
        {
            C_copie[i][j].colonne=C[i][j].colonne;
            C_copie[i][j].demande=C[i][j].demande;
            C_copie[i][j].ligne=C[i][j].ligne;
            C_copie[i][j].prod=C[i][j].prod;
            C_copie[i][j].valeur=C[i][j].valeur;
        }
    }
}
void visualiser_resultat(site* prod ,site* demande,float** transport,int n, int m,char cas[]){
// Cette proc�dure nous permet de visualiser les transitions production-demande


// Nous cr�ons une matrice de sites associ�s
    site** liaisons = NULL;
    int nb_l = 0;
// on compte le nombre de transfert, c'est � lorsqu'il y'a eu transport
    for(int i = 0;i<n;i++){
        for(int j=0;j<m;j++){
            if(transport[i][j] !=0){
                nb_l++;
            }
        }
    }
    liaisons = (site**)malloc(nb_l*sizeof(site*));
    for(int i=0;i<nb_l;i++){
        liaisons[i] = (site*)malloc(2*sizeof(site));
    }
    // remplissons le tableau
    int suiveur = 0;
    for(int i =0;i<n;i++){
        for(int j = 0;j<m;j++){
            if(transport[i][j] !=0){
                liaisons[suiveur][0] = prod[i];
                liaisons[suiveur][1] = demande[j];
                suiveur++;
            }
        }
    }
    HMGL visu = mgl_create_graph(600,600);
    mgl_set_def_param(visu);
    mgl_set_ranges(visu,0,550,0,550,0,550);
    mgl_set_axis_stl(visu,"","","");
    mgl_set_alpha_default(visu,0.5);
    mgl_set_light(visu,1);
    for(int i = 0;i<nb_l;i++){
        mgl_ball(visu,600*(liaisons[i][0].X),600*(liaisons[i][0].Y),0);
        mgl_ball(visu,600*(liaisons[i][1].X),600*(liaisons[i][1].Y),0);
        mgl_line(visu,600*(liaisons[i][0].X),600*(liaisons[i][0].Y),0,600*(liaisons[i][1].X),600*(liaisons[i][1].Y),0,"B",1);
    }
    mgl_write_jpg(visu,cas,"");
    mgl_delete_graph(visu);
    for(int i = 0;i<nb_l;i++){
        free(liaisons[i]);
    }
    free(liaisons);
}

